requires 'perl', '5.010';

on 'test' => sub {
    requires 'Test::More', '0.98';
    requires 'Test::Class::Tiny', '0.03';
};

requires 'Mojolicious'; # figure out what version
