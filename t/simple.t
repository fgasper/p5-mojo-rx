#!/usr/bin/env perl

package t::simple;

use strict;
use warnings;

use parent 'Test::Class::Tiny';

use Test2::V0;
use Test2::Tools::Explain;

use Mojo::Rx qw( rx_of  rx_merge );
use Mojo::Rx::Observable;

use constant _OBSERVABLE_CLASS => 'Mojo::Rx::Observable';

__PACKAGE__->new()->runtests() if !caller;

sub T1_unsubscribe_stops {
    my ($self) = @_;

  SKIP: {
        eval { require AnyEvent; 1 } or do {
            skip "Failed to load AnyEvent: $@", $self->num_tests();
        };

        my $timer;

        my $obs = _OBSERVABLE_CLASS()->new( sub {
            my ($emitter) = @_;

            $timer = AnyEvent->timer(
                after => 0.5,
                interval => 0.5,
                cb => sub { $emitter->next('x') },
            );

            return sub {
                diag "Tearing down …";

                $emitter->complete();
            };
        } );

        my $cv = AnyEvent->condvar();

        my @got;

        my $subscr;
        $subscr = $obs->subscribe( {
            next => sub {
                push @got, shift;

                if (@got == 3) {
                    diag 'got 3 events; waiting a bit longer before ending …';

                    undef $subscr;

                    my $t;
                    $t = AnyEvent->timer(
                        after => 1,
                        cb => sub {
                            undef $t;
                            $cv->();
                        },
                    );
                }
            },
            complete => sub { push @got, '_' },
        } );

        $cv->recv();

        is(
            \@got,
            [ qw( x x x _ ) ],
            'events as expected',
        ) or diag explain \@got;
    }
}

sub T1_event_after_unsubscribe {
    my $feed_cr;

    my $obs = _OBSERVABLE_CLASS()->new( sub {
        my ($emitter) = @_;

        $feed_cr = sub { $emitter->next(shift) };

        return;
    } );

    my @got;

    my $subsc = $obs->subscribe( {
        next => sub { push @got, shift },
        complete => sub { push @got, '_' },
    } );

    $feed_cr->(9);
    $feed_cr->(8);

    $subsc->unsubscribe();

    $feed_cr->(7);

    is(
        \@got,
        [ 9, 8 ],
        'expected events',
    ) or diag explain \@got;
}

#sub T1_warn_on_unhandled_next {
#    my $obs = _OBSERVABLE_CLASS()->new( sub {
#        my ($emitter) = @_;
#
#        $emitter->next('aaaa');
#    } );
#
#    my @w;
#    do {
#        local $SIG{'__WARN__'} = sub { push @w, @_ };
#        $obs->subscribe( {} );
#    };
#
#    is(
#        \@w,
#        [
#            match( qr<aaaa> ),
#        ],
#        'warning as expected',
#    );
#}
#
#sub T1_warn_on_unhandled_error {
#    my $obs = _OBSERVABLE_CLASS()->new( sub {
#        my ($emitter) = @_;
#
#        $emitter->error('aaaa');
#    } );
#
#    my @w;
#    do {
#        local $SIG{'__WARN__'} = sub { push @w, @_ };
#        $obs->subscribe( {} );
#    };
#
#    is(
#        \@w,
#        [
#            match( qr<aaaa> ),
#        ],
#        'warning as expected',
#    );
#}

sub T3_noerror {
    my $tore_down;

    my $obs = _OBSERVABLE_CLASS()->new( sub {
        my ($emitter) = @_;

        $emitter->next('a');
        $emitter->next('b');
        $emitter->next('c');
        $emitter->complete();

        return sub { $tore_down = 1 };
    } );

    my @got;

    my $subscr = $obs->subscribe( {
        next => sub {
            push @got, shift;
        },
        complete => sub { push @got, '__COMPLETE__' },
    } );

    is(
        \@got,
        [ qw( a b c __COMPLETE__ ) ],
        'received events and completion',
    );

    is( $tore_down, undef, 'not torn down yet' );

    $subscr->unsubscribe();

    is( $tore_down, 1, 'unsubscribe() tears down' );

    return;
}

sub T1_of {
    my @got;

    rx_of(10, 20, 30)->subscribe( {
        next => sub { push @got, shift },
        complete => sub { push @got, '__DONE' },
    } );

    is(
        \@got,
        [ 10, 20, 30, '__DONE' ],
        'result as expected',
    );
}

sub T1_merge__sync {
    my @got;

    my @obss = (
        rx_of(10, 20, 30),
        rx_of(1, 2, 3),
    );

    my $merged = rx_merge(@obss);

    $merged->subscribe( {
        next => sub { push @got, shift },
        complete => sub { push @got, '__DONE' },
    } );

    is(
        \@got,
        [ 10, 20, 30, 1, 2, 3, '__DONE' ],
        'result as expected',
    );
}

1;
