package Mojo::Rx::Subscriber;

use strict;
use warnings;

sub next {
    $_[0]->{'next'} && $_[0]->{'next'}->(@_[1 .. $#_]);
}

sub error {
    $_[0]->{'error'} && $_[0]->{'error'}->(@_[1 .. $#_]);
}

sub complete {
    $_[0]->{'complete'} && $_[0]->{'complete'}->();
}

1;
